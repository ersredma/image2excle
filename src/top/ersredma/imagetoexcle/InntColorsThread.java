package top.ersredma.imagetoexcle;
         
import java.util.Map;
         
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
         
public class InntColorsThread implements Runnable {
         
            private int rid;
            private XSSFWorkbook workbook;
            private ExcelUtil eu;
            private Map<String,XSSFCellStyle> colorMap=null;
            public InntColorsThread(int id,ExcelUtil eu) {
                this.rid=id;
                this.eu=eu;
                this.workbook=eu.getWorkbook();
            }
            @Override
    public void run() {
                //colorMap=new HashMap<String, XSSFCellStyle>();
                for(int g=0;g<15;g++){
                    for(int b=0;b<15;b++){
                        XSSFCellStyle style = workbook.createCellStyle();
                        XSSFColor xc=new XSSFColor(new byte[]{(byte)(rid!=14?rid*18:255),(byte)(g!=14?g*18:255),(byte)(b!=14?b*18:255)});
                        style.setFillForegroundColor(xc);
                        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
                        StringBuilder sb = new StringBuilder();
                        //colorMap.put(sb.append(rid).append(",").append(g).append(",").append(b).toString(),style);
                        eu.addColor(sb.append(rid).append(",").append(g).append(",").append(b).toString(),style);
                    }
                }
                //eu.addColors(colorMap);
                eu.threadOver();
                Thread.yield();
            }
         
        }