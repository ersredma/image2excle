package top.ersredma.imagetoexcle;
         
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
         
import javax.imageio.ImageIO;
         
public class ImageUtils {
         
            public static void main(String[] args) {
                // TODO Auto-generated method stub
         
            }
             
            public static BufferedImage getBfImageFromPath(String keyImagePath) {
                BufferedImage bfImage = null;
                try {
                    bfImage = ImageIO.read(new File(keyImagePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return bfImage;
            }
             
            public static  int[][][] getImageGRB(BufferedImage image) {
                int width = image.getWidth();
                int height = image.getHeight();
                int[][][] result = new int[height][width][3];
                for (int x = 0; x < height; x++) {
                    for (int y = 0; y < width; y++) {
                        int pixel = image.getRGB(y, x);
                        result[x][y][0] = (pixel & 0xff0000) >> 16;
                        result[x][y][1] = (pixel & 0xff00) >> 8;
                        result[x][y][2] = (pixel & 0xff);
                    }
                }
                return result;
            }
            /**
      * 生成缩略图 <br/>
      * 保存:ImageIO.write(BufferedImage, imgType[jpg/png/...], File);
      * 
      * @param source
      *            原图片
      * @param width
      *            缩略图宽
      * @param height
      *            缩略图高
      * @param b
      *            是否等比缩放
      * */
         
            public static BufferedImage thumb(BufferedImage source, int width,
                    int height, boolean b) {
                // targetW，targetH分别表示目标长和宽
         
                int type = source.getType();
                BufferedImage target = null;
                double sx = (double) width / source.getWidth();
                double sy = (double) height / source.getHeight();
         
                if (b) {
                    if (sx > sy) {
                        sx = sy;
                        width = (int) (sx * source.getWidth());
                    } else {
                        sy = sx;
                        height = (int) (sy * source.getHeight());
                    }
                }
         
                if (type == BufferedImage.TYPE_CUSTOM) { // handmade
                    ColorModel cm = source.getColorModel();
                    WritableRaster raster = cm.createCompatibleWritableRaster(width,
                            height);
                    boolean alphaPremultiplied = cm.isAlphaPremultiplied();
                    target = new BufferedImage(cm, raster, alphaPremultiplied, null);
                } else {
                    target = new BufferedImage(width, height, type);
                }
                Graphics2D g = target.createGraphics();
                // smoother than exlax:
                g.setRenderingHint(RenderingHints.KEY_RENDERING,
                        RenderingHints.VALUE_RENDER_QUALITY);
                g.drawRenderedImage(source, AffineTransform.getScaleInstance(sx, sy));
                g.dispose();
                return target;
            }
         
        }